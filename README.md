PureScript container with bower
===============================

**Note: bower shouldn't be used for new projects.**

I use this container to do exercises on https://exercism.io/ in the PureScript
track.

```sh
docker run -it -v ${HOME}/exercism:/home/purescript/exercism mawis/purescript-bower
```

Notes on the PureScript exercises at exercism.io
------------------------------------------------

The process is:

```sh
su - purescript
cd exercism/hello-world
vi bower.json
bower install
# edit the solution
pulp build
pulp test
```
